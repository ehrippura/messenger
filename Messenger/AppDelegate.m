//
//  AppDelegate.m
//  Messenger
//
//  Created by Tzu-Yi Lin on 2015/5/29.
//  Copyright (c) 2015年 Tzu-Yi Lin. All rights reserved.
//

#import "AppDelegate.h"
#import "WebViewDelegate.h"

@interface AppDelegate () {
    WebViewDelegate *webDelegate;
}
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application

    webDelegate = [WebViewDelegate new];

    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    [config.userContentController addScriptMessageHandler:webDelegate name:@"handler"];

    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];

    [self.window setContentView:self.webView];
    NSURLRequest *r = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://messenger.com"]];
    [self.webView loadRequest:r];

    self.webView.UIDelegate = webDelegate;
    self.webView.navigationDelegate = webDelegate;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag
{
    if (!flag) {
        [self.window makeKeyAndOrderFront:nil];
    }

    return YES;
}

@end
