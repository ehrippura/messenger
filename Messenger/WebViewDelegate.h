//
//  WebViewDelegate.h
//  Messenger
//
//  Created by Tzu-Yi Lin on 2015/5/29.
//  Copyright (c) 2015年 Tzu-Yi Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

@import WebKit;

@interface WebViewDelegate : NSObject <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@end
