//
//  AppDelegate.h
//  Messenger
//
//  Created by Tzu-Yi Lin on 2015/5/29.
//  Copyright (c) 2015年 Tzu-Yi Lin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@import WebKit;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property(strong)  WKWebView *webView;

@end

